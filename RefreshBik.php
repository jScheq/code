<?php
/**
 * Created by PhpStorm.
 * User: jscheq
 * Date: 28.06.17
 * Time: 6:51
 */

namespace app\models;
use DOMDocument;
use DOMXPath;
use SimpleXMLElement;

/**
 * Class RefreshBik
 * Класс для актуализации базы банков
 *
 * @package app\models
 */
class RefreshBik
{
    const DOWNLOAD_PATH = 'http://www.bik-info.ru/base/base.xml';
    const SAVE_PATH = 'tmp/bik/';
    const FILE_NAME = 'bik.xml';

    private static $xPath;
    private static $root;
    private static $objSXE = null;

    /**
     * Загружает XML файл
     */
    public static function downloadXMLBik()
    {
        $file = file_get_contents(self::DOWNLOAD_PATH);

        file_put_contents(self::SAVE_PATH . self::FILE_NAME, $file);
    }

    public static function init()
    {
        self::loadXMLInRAM();
    }

    /**
     * Загружает DOMXPath и проставялет root и xPath
     */
    public static function loadXMLInRAM()
    {
        // Создаём XML-документ версии 1.0 с кодировкой utf-8
        $dom = new domDocument( "1.0", "windows-1251" );

        // Загружаем XML-документ из файла в объект DOM
        $dom->load( self::SAVE_PATH . self::FILE_NAME );

        // Получаем корневой элемент
        self::$root = $dom->documentElement;
        self::$xPath = new DOMXPath($dom);
    }

    /**
     * Получает текущую версию базы банков
     *
     * @return mixed - 28.06.2017
     */
    public static function getCurrentVersion()
    {
        $entries = self::$xPath->query("//@version", self::$root);
        $actualVersion = $entries->item(0)->nodeValue;
        return $actualVersion;
    }

    /**
     * Инициализирует объект $objSXE типа SimpleXMLElement с данными биков
     */
    public static function initSXE()
    {
        $file = file_get_contents(self::SAVE_PATH . self::FILE_NAME);
        self::$objSXE = new SimpleXMLElement($file);
    }

    /**
     * Перед началом использования требуется  вызвать метод initSXE()
     *
     * @return mixed
     */
    public static function getBikData()
    {
        if (self::$objSXE == null) return false;

        return self::$objSXE;
    }
}